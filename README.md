# Infrastructure
## Version
OpenTofu v1.6.0-beta3 \
on darwin_amd64
+ provider registry.opentofu.org/opentofu/google v5.8.0

Google Cloud SDK 456.0.0

## Quick start
Connect gcloud cli and select default project: \
```gcloud auth application-default login```\
```gcloud config set project kakashiel-407514```
```brew install opentofu```
### tofu command
```tofu init```\
```tofu plan -var-file=environments/prod/terraform.tfvars```\
```tofu apply -var-file=environments/prod/terraform.tfvars```\
```tofu destroy -var-file=environments/prod/terraform.tfvars```

### TODO
- [ ] Fix ci/cd
### Done
- price less than 0.10 dollars a month
- Terraform best practices
- Straightforward new environment
- HTTPS
