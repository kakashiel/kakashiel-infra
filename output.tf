output "project_id" {
  value = var.project_id
}
output "region" {
  value = var.region
}
output "environment" {
  value = var.environment
}

output "cloud_artifact_registry_repository_url" {
  value = module.cloud_artifact_registry.repository_url
}

output "cloud_artifact_registry_repository_name" {
  value = module.cloud_artifact_registry.repository_name
}

output "cloud_run_service_url" {
  value = module.cloud_run.cloud_run_service_url
}