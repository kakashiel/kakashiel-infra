terraform {
  required_version = ">= 1.4"

  backend "gcs" {
    bucket  = "tf-state-kakashiel-backend"
    prefix  = "terraform/states"
  }
}

module "cloud_artifact_registry" {
  source  = "./modules/gcp/cloud_artifact"
  location  = var.region
  project_id = var.project_id
  environment = var.environment
  user_email = "adrien.loustaunau@gmail.com"
  project_name = var.project_name
  service_account_name = var.service_account_name
#  service_account_key_file = var.service_account_key_file
}
module "cloud_run" {
  source  = "./modules/gcp/cloud_run"
  location  = var.region
  project_id = var.project_id
  environment = var.environment
  project_name = var.project_name
  service_account_key_file = var.service_account_key_file
  image = "${module.cloud_artifact_registry.repository_url}/backend:latest"
}
