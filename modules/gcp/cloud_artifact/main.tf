resource "google_artifact_registry_repository" "repository" {
  provider      = google
  location      = var.location
  repository_id = "${var.project_name}-${var.environment}-repository"
  format        = "DOCKER"
  cleanup_policies {
    id     = "delete-old-images"
    action = "DELETE"
    condition {
      tag_state  = "UNTAGGED"
      older_than = "1d"  # 1 day in seconds
    }
  }
}

resource "google_artifact_registry_repository_iam_policy" "policy" {
  provider = google
  project  = var.project_id
  location = var.location
  repository = google_artifact_registry_repository.repository.repository_id

  policy_data = <<EOF
{
  "bindings": [
    {
      "role": "roles/artifactregistry.writer",
      "members": [
        "serviceAccount:${var.service_account_name}@${var.project_id}.iam.gserviceaccount.com",
        "user:${var.user_email}"
      ]
    }
  ]
}
EOF
}
