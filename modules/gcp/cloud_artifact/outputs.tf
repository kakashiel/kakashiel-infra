output "repository_name" {
  description = "The name of the repository."
  value       = google_artifact_registry_repository.repository.repository_id
}

output "repository_url" {
  description = "The URL of the repository."
  value       = "${var.location}-docker.pkg.dev/${var.project_id}/${google_artifact_registry_repository.repository.repository_id}"
}

output "repository_location" {
  description = "The location of the repository."
  value       = google_artifact_registry_repository.repository.location
}