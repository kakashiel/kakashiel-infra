variable "location" {
  description = "Google Cloud region"
}

variable "project_name" {
  description = "Name of the project"
}

variable "project_id" {
  description = "Project ID"
}
variable "environment" {
  description = "Environment"
}

variable "user_email" {
    description = "User email"
}

variable "service_account_name" {
    description = "Service account name"
}