variable "location" {
  description = "Google Cloud region"
}
variable "project_id" {
  description = "Project ID"
}
variable "environment" {
  description = "Environment"
}

variable "project_name" {
  description = "Service name use"
}

variable "service_account_key_file" {
    description = "Service account key file"
}

variable "image" {
  description = "Docker image"
  default =  "gcr.io/cloudrun/hello"
#  default = "gcr.io/${var.project_id}/${var.project_name}:${var.environment}"
}
