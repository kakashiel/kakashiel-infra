module "cloud_run" {
  source  = "GoogleCloudPlatform/cloud-run/google"
  version = "~> 0.10.0"

  # Required variables
  service_name = "${var.project_name}-${var.environment}"
  project_id   = var.project_id
  location     = var.location
  image        = var.image
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = var.location
  project     = var.project_id
  service     = module.cloud_run.service_name

  policy_data = data.google_iam_policy.noauth.policy_data
}
