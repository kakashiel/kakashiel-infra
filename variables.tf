variable "region" {
  description = "Google Cloud region for production"
  default     = "us-central1"
}

variable "project_name" {
  description = "Name of the project"
  default     = "kakashiel"
}

variable "project_id" {
    description = "Project ID"
}

variable "environment" {
    description = "Environment"
    default     = "dev"
}

variable "service_account_key_file" {
  description = "Service account key file"
}
variable "service_account_name" {
    description = "Service account name"
}
